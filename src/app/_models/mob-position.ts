export interface MobPosition {
  group: string;
  world: string;
  x: number;
  y: number;
  z: number;
  yaw: number;
  pitch: number;
}
