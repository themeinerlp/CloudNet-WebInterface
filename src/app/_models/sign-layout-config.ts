/*
 * Copyright (c) 2019.
 * CloudNet-WebInterface von Phillipp Glanz ist lizenziert unter einer Creative Commons
 *  Namensnennung -
 *  Nicht kommerziell -
 *  Keine Bearbeitungen 4.0 International Lizenz.
 */

import {SignGroupLayouts} from './sign-group-layouts';
import {SearchingAnimation} from './searching-animation';

export interface SignLayoutConfig {
  fullServerHide: boolean;
  knockbackOnSmallDistance: boolean;
  distance: number;
  strength: number;
  groupLayouts: SignGroupLayouts[];
  searchingAnimation: SearchingAnimation;
}
