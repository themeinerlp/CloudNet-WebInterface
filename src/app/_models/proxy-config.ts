/*
 * Copyright (c) 2019.
 * CloudNet-WebInterface von Phillipp Glanz ist lizenziert unter einer Creative Commons
 *  Namensnennung -
 *  Nicht kommerziell -
 *  Keine Bearbeitungen 4.0 International Lizenz.
 */

import {Motd} from './motd';
import {TabList} from './tab-list';
import {AutoSlot} from './auto-slot';
import {DynamicFallback} from './dynamic-fallback';


export interface ProxyConfig {
  enabled: boolean;
  maintenance: boolean;
  motdsLayouts: Array<Motd>;
  maintenanceMotdLayout: Motd;
  maintenaceProtocol: string;
  maxPlayers: number;
  fastConnect: boolean;
  customPayloadFixer: boolean;
  autoSlot: AutoSlot;
  tabList: TabList;
  playerInfo: string[];
  whitelist: Array<string>;
  dynamicFallback: DynamicFallback;

}
