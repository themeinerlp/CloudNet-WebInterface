export interface KeyValue<T, T2> {
  key: T;
  value: T2;
}
