/*
 * Copyright (c) 2019.
 * CloudNet-WebInterface von Phillipp Glanz ist lizenziert unter einer Creative Commons
 *  Namensnennung -
 *  Nicht kommerziell -
 *  Keine Bearbeitungen 4.0 International Lizenz.
 */

import {PermissionEntity} from './permission-entity';
import {PlayerConnection} from './player-connection';

export interface OfflinePlayer {
  uniqueId: string;
  name: string;
  lastLogin: number;
  firstLogin: number;
  lastPlayerConnection: PlayerConnection;
  permissionEntity: PermissionEntity;
}
