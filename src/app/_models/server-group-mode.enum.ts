/*
 * Copyright (c) 2019.
 * CloudNet-WebInterface von Phillipp Glanz ist lizenziert unter einer Creative Commons
 *  Namensnennung -
 *  Nicht kommerziell -
 *  Keine Bearbeitungen 4.0 International Lizenz.
 */

export enum ServerGroupMode {
  // noinspection JSUnusedGlobalSymbols
  // noinspection JSUnusedGlobalSymbols
  LOBBY = 'LOBBY',
  STATIC = 'STATIC',
  DYNAMIC = 'DYNAMIC',
  STATIC_LOBBY = 'STATIC_LOBBY'
}
