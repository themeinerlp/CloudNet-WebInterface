export interface MobItemLayout {
  itemId: number;
  subId: number;
  itemName: string;
  display: string;
  lore: string[];
}
