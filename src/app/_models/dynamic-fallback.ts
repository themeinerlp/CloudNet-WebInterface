/*
 * Copyright (c) 2019.
 * CloudNet-WebInterface von Phillipp Glanz ist lizenziert unter einer Creative Commons
 *  Namensnennung -
 *  Nicht kommerziell -
 *  Keine Bearbeitungen 4.0 International Lizenz.
 */

import {ServerFallback} from './server-fallback';

/**
 * Gibt an welche Server Gruppe Standard Fallback ist
 * @fallbacks - Gibt an welche Fallbacks zuständig sind
 */
export interface DynamicFallback {
  defaultFallback: string;
  fallbacks: Array<ServerFallback>;
}
