/*
 * Copyright (c) 2019.
 * CloudNet-WebInterface von Phillipp Glanz ist lizenziert unter einer Creative Commons
 *  Namensnennung -
 *  Nicht kommerziell -
 *  Keine Bearbeitungen 4.0 International Lizenz.
 */

import {Template} from './template';
import {ProxyVersion} from './proxy-version.enum';
import {ProxyConfig} from './proxy-config';
import {ProxyGroupMode} from './proxy-group-mode.enum';

export interface ProxyGroup {
  name: string;
  wrapper: Array<string>;
  template: Template;
  proxyVersion: ProxyVersion;
  startPort: number;
  startup: number;
  memory: number;
  proxyConfig: ProxyConfig;
  proxyGroupMode: ProxyGroupMode;

}
