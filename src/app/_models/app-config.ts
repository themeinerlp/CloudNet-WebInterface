/*
 * Copyright (c) 2019.
 * CloudNet-WebInterface von Phillipp Glanz ist lizenziert unter einer Creative Commons
 *  Namensnennung -
 *  Nicht kommerziell -
 *  Keine Bearbeitungen 4.0 International Lizenz.
 */

import {CloudNetWork} from './cloud-net-work';
import {VersionType} from './version-type.enum';
import {Recaptcha} from './recaptcha';
import {Style} from './style';
import {Settings} from './settings';
import {Analytics} from './analytics';

/**
 *
 */
export interface IAppConfig {
  Servers: CloudNetWork[];
  updateChannel: VersionType;
  GoogleRecaptcha: Recaptcha;
  style: Style;
  settings: Settings;
  analytics: Analytics;
}
