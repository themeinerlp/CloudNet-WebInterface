/*
 * Copyright (c) 2019.
 * CloudNet-WebInterface von Phillipp Glanz ist lizenziert unter einer Creative Commons
 *  Namensnennung -
 *  Nicht kommerziell -
 *  Keine Bearbeitungen 4.0 International Lizenz.
 */

import {ServiceId} from './service-id';

export interface SimpleServerGroup {
  serviceId: ServiceId;
  hostName: string;
  port: number;
  maxplayers: number;
  onlineCount: number;
}
