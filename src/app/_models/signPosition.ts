export interface SignPosition {
  group: string;
  world: string;
  x: number;
  y: number;
  z: number;
}
