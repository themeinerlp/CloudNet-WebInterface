/*
 * Copyright (c) 2019.
 * CloudNet-WebInterface von Phillipp Glanz ist lizenziert unter einer Creative Commons
 *  Namensnennung -
 *  Nicht kommerziell -
 *  Keine Bearbeitungen 4.0 International Lizenz.
 */

import {SignLayout} from './sign-layout';

export interface SearchingAnimation {
  animations: number;
  animationsPerSecond: number;
  searchingLayouts: SignLayout[];
}
