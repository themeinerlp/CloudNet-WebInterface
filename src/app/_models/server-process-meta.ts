/*
 * Copyright (c) 2019.
 * CloudNet-WebInterface von Phillipp Glanz ist lizenziert unter einer Creative Commons
 *  Namensnennung -
 *  Nicht kommerziell -
 *  Keine Bearbeitungen 4.0 International Lizenz.
 */

import {ServiceId} from './service-id';
import {ServerInstallablePlugin} from './server-installable-plugin';
import {ServerConfig} from './server-config';
import {Template} from './template';

export interface ServerProcessMeta {
  serviceId: ServiceId;
  memory: number;
  priorityStop: boolean;
  url: string;
  processParameters: string[];
  onlineMode: boolean;
  downloadablePlugins: ServerInstallablePlugin[];
  serverConfig: ServerConfig;
  customServerDownload: string;
  port: number;
  template: Template;
}
