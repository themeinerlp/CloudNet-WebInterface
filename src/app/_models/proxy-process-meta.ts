/*
 * Copyright (c) 2019.
 * CloudNet-WebInterface von Phillipp Glanz ist lizenziert unter einer Creative Commons
 *  Namensnennung -
 *  Nicht kommerziell -
 *  Keine Bearbeitungen 4.0 International Lizenz.
 */

import {ServiceId} from './service-id';
import {ServerInstallablePlugin} from './server-installable-plugin';

export interface ProxyProcessMeta {
  serviceId: ServiceId;
  memory: number;
  port: number;
  processParameters: string[];
  url: string;
  downloadablePlugins: ServerInstallablePlugin[];
}
