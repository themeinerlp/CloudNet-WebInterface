import {MobPosition} from './mob-position';

export interface ServerMob {
  uniqueId: string;
  display: string;
  name: string;
  itemName: string;
  type: string;
  targetGroup: string;
  itemId: number;
  autoJoin: boolean;
  position: MobPosition;
  displayMessage: string;
  metaDatDoc: string;
}
