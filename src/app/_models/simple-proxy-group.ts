/*
 * Copyright (c) 2019.
 * CloudNet-WebInterface von Phillipp Glanz ist lizenziert unter einer Creative Commons
 *  Namensnennung -
 *  Nicht kommerziell -
 *  Keine Bearbeitungen 4.0 International Lizenz.
 */

import {ServiceId} from './service-id';

export interface SimpleProxyGroup {
  serviceId: ServiceId;
  online: boolean;
  hostName: string;
  port: number;
  memory: number;
  onlineCount: number;
}
