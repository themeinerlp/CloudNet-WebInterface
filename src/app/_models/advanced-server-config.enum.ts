/*
 * Copyright (c) 2019.
 * CloudNet-WebInterface von Phillipp Glanz ist lizenziert unter einer Creative Commons
 *  Namensnennung -
 *  Nicht kommerziell -
 *  Keine Bearbeitungen 4.0 International Lizenz.
 */

/**
 *
 * Gibt an ob eine benachrichtigung für ein Proxy Update gegebn werden soll
 * Gibt an ob eine benachrichtigung für ein Server Update gegebn werden soll
 * Deaktiviert das Automatische Speichern von Welten
 *
 */
export interface AdvancedServerConfig {
  notifyPlayerUpdatesFromNoCurrentPlayer: boolean;
  notifyProxyUpdates: boolean;
  notifyServerUpdates: boolean;
  disableAutoSavingForWorlds: boolean;
}
