import {SignPosition} from './signPosition';

export interface Sign {
  uniqueId: string;
  targetGroup: string;
  position: SignPosition;
}
