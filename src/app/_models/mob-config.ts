import {KeyValue} from './key-value';
import {MobItemLayout} from './mob-item-layout';

export interface MobConfig {
  inventorySize: number;
  startPoint: number;
  itemLayout: MobItemLayout;
  defaultItemInventory: KeyValue<number, MobItemLayout>[];
}
