/*
 * Copyright (c) 2019.
 * CloudNet-WebInterface von Phillipp Glanz ist lizenziert unter einer Creative Commons
 *  Namensnennung -
 *  Nicht kommerziell -
 *  Keine Bearbeitungen 4.0 International Lizenz.
 */

import {Component, OnInit, ViewChild} from '@angular/core';
import {AuthenticationService} from '../../_services/authentication.service';
import {ActivatedRoute, Router} from '@angular/router';
import {CloudNetWork} from '../../_models';
import {AppConfigService} from '../../_services/app-config.service';
import {FormControl, Validators} from '@angular/forms';
import {NotificationsService} from '../../_services/notifications.service';
import {CookieService} from 'ngx-cookie-service';
import {TranslateService} from '@ngx-translate/core';
import {InvisibleReCaptchaComponent} from 'ngx-captcha';
import {ElectronService} from 'ngx-electron';
import {HttpErrorResponse} from '@angular/common/http';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],

})
export class LoginComponent implements OnInit {
  model: any = {};
  loading = false;
  returnUrl: string;
  sitekey: string;
  captcha;
  networks: CloudNetWork[] = [];
  networkcontrol = new FormControl('', [Validators.required]);
  @ViewChild('captchaElem') captchaElem: InvisibleReCaptchaComponent;
  public recaptcha: any = null;
  public ipConnect = false;
  ssl = false;
  cPort = false;
  ip: string;
  cUrl = false;
  port: number;
  url: string;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
    private _notify: NotificationsService,
    private config: AppConfigService,
    private cookie: CookieService,
    private translate: TranslateService,
    private _es: ElectronService) {
    this.translate.setDefaultLang('en');
    this.translate.use(this.translate.getBrowserLang());
  }

  ngOnInit() {
    // reset login status
    this.authenticationService.logout();

    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';

    setTimeout(() => {
      this.config.load();
      this.networks = AppConfigService.settings.Servers;
      this.sitekey = AppConfigService.settings.GoogleRecaptcha.SiteKey;
      this.captcha = AppConfigService.settings.GoogleRecaptcha.enabled;
    }, 150);
  }


  resolved() {
    if (!this.ipConnect) {
      AppConfigService.settings.Servers.forEach(t => {
        if (t.CloudName === this.networkcontrol.value) {
          if (this._es.isElectronApp) {
            localStorage.setItem('currentCloud', JSON.stringify(t));
          } else {
            this.cookie.set('currentCloud', JSON.stringify(t));
          }
        }
      });
    } else {
      const network: CloudNetWork = {
        CloudURL: (this.ssl ? 'https://' : 'http://') + this.ip + ':' + (this.cPort ? this.port : '1420') + (this.cUrl ? this.url : ''),
        CloudName: 'IPCONNECT'
      };
      if (this._es.isElectronApp) {
        localStorage.setItem('currentCloud', JSON.stringify(network));
      } else {
        this.cookie.set('currentCloud', JSON.stringify(network));
      }
    }

    if (AppConfigService.settings.GoogleRecaptcha.enabled) {
      this.loading = true;

      this.authenticationService.login(this.model.username, this.model.password)
        .subscribe(
          (t) => {
            const d = new Date();
            if (this._es.isElectronApp) {
              localStorage.setItem('currentUser', JSON.stringify(t.response));
            } else {
              this.cookie.set('currentUser', JSON.stringify(t.response),
                new Date(d.getTime() + AppConfigService.settings.settings.timeout * 60 * 1000), '/');
            }
            AuthenticationService.isUserLoggedIn.next(true);
            this.router.navigate([this.returnUrl]).catch();
          },
          error => {
            if (error.status === 401) {
              this._notify.LoginError();
            }
            this.loading = false;
          });
    }
  }

  login() {
    if (!this.ipConnect) {
      AppConfigService.settings.Servers.forEach(t => {
        if (t.CloudName === this.networkcontrol.value) {
          if (this._es.isElectronApp) {
            localStorage.setItem('currentCloud', JSON.stringify(t));
          } else {
            this.cookie.set('currentCloud', JSON.stringify(t));
          }

        }
      });
    } else {
      const network: CloudNetWork = {
        CloudURL: (this.ssl ? 'https://' : 'http://') + this.ip + ':' + (this.cPort ? this.port : '1420') + (this.cUrl ? this.url : ''),
        CloudName: 'IPCONNECT'
      };
      if (this._es.isElectronApp) {
        localStorage.setItem('currentCloud', JSON.stringify(network));
      } else {
        this.cookie.set('currentCloud', JSON.stringify(network));
      }
    }
    if (!AppConfigService.settings.GoogleRecaptcha.enabled) {
      this.loading = true;

      this.authenticationService.login(this.model.username, this.model.password)
        .subscribe(
          (t) => {
            const d = new Date();
            if (this._es.isElectronApp) {
              localStorage.setItem('currentUser', JSON.stringify(t.response));
            } else {
              this.cookie.set('currentUser', JSON.stringify(t.response),
                new Date(d.getTime() + AppConfigService.settings.settings.timeout * 60 * 1000), '/');
            }
            AuthenticationService.isUserLoggedIn.next(true);
            this.router.navigate([this.returnUrl]).catch();
          },
          error => {

            if (error instanceof HttpErrorResponse) {
              if (error.status === 401) {
                this._notify.LoginError();
              }
              if (error.status === 0) {
                this._notify.UnknownError();
              }
              console.log(JSON.stringify(error));
            }
            this.loading = false;
          });
    }
  }

  isGoogleRecaptcha() {
    if (this._es.isElectronApp) {
      return false;
    }
    return this.captcha;
  }
}
