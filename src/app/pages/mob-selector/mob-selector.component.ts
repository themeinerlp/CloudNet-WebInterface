import {Component, OnInit, ViewChild} from '@angular/core';
import {CloudConnectorService} from '../../_services/cloud-connector.service';
import {MatDialog, MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {NotificationsService} from '../../_services/notifications.service';
import {CookieService} from 'ngx-cookie-service';
import {ElectronService} from 'ngx-electron';
import {MatomoTracker} from 'ngx-matomo';
import {AppConfigService} from '../../_services/app-config.service';

import {MinecraftServer} from '../../_models/minecraft-server';
import {MobConfig} from '../../_models/mob-config';
import {ServerState} from '../../_models/server-state.enum';
import {TemplateResource} from '../../_models/template-resource';
import {ServerGroupType} from '../../_models/server-group-type.enum';
import {MobItemLayout} from '../../_models/mob-item-layout';
import {KeyValue} from '../../_models/key-value';
import {EditItemComponent} from '../../popup/edit-item/edit-item.component';
import {ServerMob} from '../../_models/server-mob';
import {EditMobComponent} from '../../popup/edit-mob/edit-mob.component';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-mob-selector',
  templateUrl: './mob-selector.component.html',
  styleUrls: ['./mob-selector.component.css']
})
export class MobSelectorComponent implements OnInit {


  mobConfig: MobConfig = {
    inventorySize: 54,
    startPoint: 10,
    itemLayout: {
      itemId: 388,
      subId: 0,
      display: '§6%server%',
      itemName: ' ',
      lore: [
        ' ',
        '§e%state%',
        '§e%online_players% §8/§e%max_players%',
        '§e%motd%'
      ]
    },
    defaultItemInventory: [
      {
        key: 1,
        value:
          {
            itemId: 160,
            subId: 15,
            display: ' ',

            itemName: ' ',
            lore: [
              ' '
            ]
          }
      },
      {
        key: 2,
        value:
          {
            itemId: 160,
            subId: 15,
            display: ' ',

            itemName: ' ',
            lore: [
              ' '
            ]
          }
      },
      {
        key: 3,
        value:
          {
            itemId: 160,
            subId: 15,
            display: ' ',

            itemName: ' ',
            lore: [
              ' '
            ]
          }
      },
      {
        key: 4,
        value:
          {
            itemId: 160,
            subId: 15,
            display: ' ',

            itemName: ' ',
            lore: [
              ' '
            ]
          }
      },
      {
        key: 5,
        value:
          {
            itemId: 160,
            subId: 15,
            display: ' ',

            itemName: ' ',
            lore: [
              ' '
            ]
          }
      },
      {
        key: 6,
        value:
          {
            itemId: 160,
            subId: 15,
            display: ' ',

            itemName: ' ',
            lore: [
              ' '
            ]
          }
      },
      {
        key: 7,
        value:
          {
            itemId: 160,
            subId: 15,
            display: ' ',

            itemName: ' ',
            lore: [
              ' '
            ]
          }
      },
      {
        key: 8,
        value:
          {
            itemId: 160,
            subId: 15,
            display: ' ',
            itemName: ' ',
            lore: [
              ' '
            ]
          }
      },
      {
        key: 9,
        value:
          {
            itemId: 160,
            subId: 15,
            display: ' ',
            itemName: ' ',
            lore: [
              ' '
            ]
          }
      }
    ]
  };

  private server: MinecraftServer = {
    serverInfo: {
      memory: -1,
      port: -1,
      maxPlayers: -1,
      onlineCount: -1,
      motd: 'A Minecraft Server',
      host: '127.0.0.1',
      serverState: ServerState.LOBBY,
      players: [],
      online: true,
      serverConfig: {
        extra: false,
        hideServer: false,
        startup: -1
      },
      serviceId: {
        group: 'Unknown',
        wrapperId: 'Unknown Wrapper',
        id: -1,
        serverId: 'Unknown ID',
        uniqueId: 'UUID Not fround'
      },
      template: {
        name: 'Template not Found',
        backend: TemplateResource.LOCAL,
        intallablePlugins: [],
        processPreParameters: [],
        url: ''
      }
    },
    groupMode: ServerGroupType.BUKKIT,
    processMeta: {
      onlineMode: true,
      customServerDownload: '',
      downloadablePlugins: [],
      memory: -1,
      port: -1,
      priorityStop: false,
      processParameters: [],
      serverConfig: {
        extra: false,
        hideServer: false,
        startup: -1
      },
      serviceId: {
        group: 'Unknown',
        wrapperId: 'Unknown Wrapper',
        id: -1,
        serverId: 'Unknown ID',
        uniqueId: 'UUID Not fround'
      },
      template: {
        name: 'Template not Found',
        backend: TemplateResource.LOCAL,
        intallablePlugins: [],
        processPreParameters: [],
        url: ''
      },
      url: ''
    },
    serviceId: {
      group: 'Unknown',
      wrapperId: 'Unknown Wrapper',
      id: -1,
      serverId: 'Unknown ID',
      uniqueId: 'UUID Not fround'
    }
  };
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  mobs: MatTableDataSource<MobDB> = null;
  displayedColumns: string[] = ['display', 'name', 'type', 'targetGroup', 'displayMessage', 'autoJoin', 'edit'];

  constructor(private _cloud: CloudConnectorService, public dialog: MatDialog, public _notify: NotificationsService,
              private cookie: CookieService, private _es: ElectronService, private matomoTracker: MatomoTracker,
              private _: TranslateService) {
    if (AppConfigService.settings.analytics.enabled === true) {
      this.matomoTracker.trackEvent('page', 'mob');
    }
  }

  ngOnInit() {


    setTimeout(() => {
      this._cloud.getMobConfig(JSON.parse(this._es.isElectronApp ? localStorage.getItem('currentUser') : this.cookie.get('currentUser')))
        .subscribe((t) => {
          this.mobConfig = JSON.parse(t['response']) as MobConfig;
      }, error => {
        if ((error.status === 403)) {
          this._notify.AccessDenied();
        }
      });
      this.refreshItems();

      this._cloud.getRandomServer(JSON.parse(this._es.isElectronApp ? localStorage.getItem('currentUser') : this.cookie.get('currentUser')))
        .subscribe((t) => {
        this.server = (JSON.parse(t['response'])) as MinecraftServer;
      }, error => {
        if ((error.status === 403)) {
          this._notify.AccessDenied();
        }
      });

    }, 200);

  }

  refreshItems() {
    this._cloud.getMobDB(JSON.parse(this._es.isElectronApp ? localStorage.getItem('currentUser') : this.cookie.get('currentUser')))
      .subscribe((response) => {
      const mob: MobDB[] = [];
      (response['response'] as string[]).forEach(s => {
        const ss = JSON.parse(s) as ServerMob;
        mob.push({
          autoJoin: ss.autoJoin,
          display: ss.display,
          displayMessage: ss.displayMessage,
          name: ss.name,
          type: ss.type,
          targetGroup: ss.targetGroup,
          mob: ss
        });
      });
      this.mobs = new MatTableDataSource(mob);
      this.mobs.sort = this.sort;
      this.mobs.paginator = this.paginator;
    });
  }

  replaceColor(value: string) {

    return value
      .replace(/&4/g, '<span style="color: #AA0000; text-shadow: 1px 1px 0 #2A0000;" >')
      .replace(/&c/g, '<span style="color: #FF5555; text-shadow: 1px 1px 0 #3F1515;" >')
      .replace(/&6/g, '<span style="color: #FFAA00; text-shadow: 1px 1px 0 #2A2A00;" >')
      .replace(/&e/g, '<span style="color: #FFFF55; text-shadow: 1px 1px 0 #3F3F15;" >')
      .replace(/&2/g, '<span style="color: #00AA00; text-shadow: 1px 1px 0 #002A00;" >')
      .replace(/&a/g, '<span style="color: #55FF55; text-shadow: 1px 1px 0 #153F15;" >')
      .replace(/&b/g, '<span style="color: #55FFFF; text-shadow: 1px 1px 0 #153F3F;" >')
      .replace(/&3/g, '<span style="color: #00AAAA; text-shadow: 1px 1px 0 #002A2A;" >')
      .replace(/&1/g, '<span style="color: #0000AA; text-shadow: 1px 1px 0 #00002A;" >')
      .replace(/&9/g, '<span style="color: #5555FF; text-shadow: 1px 1px 0 #15153F;" >')
      .replace(/&d/g, '<span style="color: #FF55FF; text-shadow: 1px 1px 0 #3F153F;" >')
      .replace(/&5/g, '<span style="color: #AA00AA; text-shadow: 1px 1px 0 #2A002A;" >')
      .replace(/&7/g, '<span style="color: #AAAAAA; text-shadow: 1px 1px 0 #2A2A2A;" >')
      .replace(/&8/g, '<span style="color: #555555; text-shadow: 1px 1px 0 #151515;" >')
      .replace(/&0/g, '<span style="color: #000000; text-shadow: 1px 1px 0 #000000;" >')
      .replace(/&f/g, '<span style="color: #FFFFFF; text-shadow: 1px 1px 0 #3F3F3F;" >')

      .replace(/&l/g, '<span style="font-weight: bold">')
      .replace(/&n/g, '<span style="text-decoration: underline">')
      .replace(/&o/g, '<span style="font-style: italic;">')
      .replace(/&m/g, '<span style="text-decoration: line-through">')
      .replace(/&r/g, '<span style="color: white; text-decoration: none; font-weight: normal; font-style: normal">')
      .replace(/§4/g, '<span style="color: #fe0000">')
      .replace(/§c/g, '<span style="color: #ff4c52">')
      .replace(/§6/g, '<span style="color: #ff8e42">')
      .replace(/§e/g, '<span style="color: #ffd800">')
      .replace(/§2/g, '<span style="color: #31a310">')
      .replace(/§a/g, '<span style="color: #4dd800">')
      .replace(/§b/g, '<span style="color: #01ffff">')
      .replace(/§3/g, '<span style="color: #0094fe">')
      .replace(/§1/g, '<span style="color: #0026ff">')
      .replace(/§9/g, '<span style="color: #4368ff">')
      .replace(/§d/g, '<span style="color: #fe00dc">')
      .replace(/§5/g, '<span style="color: #b100fe">')
      .replace(/§7/g, '<span style="color: #a0a0a0">')
      .replace(/§8/g, '<span style="color: #404040">')
      .replace(/§0/g, '<span style="color: #010101">')
      .replace(/§f/g, '<span style="color: #ffffff">')

      .replace(/§l/g, '<span style="font-weight: bold">')
      .replace(/§n/g, '<span style="text-decoration: underline">')
      .replace(/§o/g, '<span style="font-style: italic;">')
      .replace(/§m/g, '<span style="text-decoration: line-through">')
      .replace(/§r/g, '<span style="color: white; text-decoration: none; font-weight: normal; font-style: normal">')
      .replace(/%server%/g, this.server.serverInfo.serviceId.serverId)
      .replace(/%state%/g, this.server.serverInfo.serverState)
      .replace(/%online_players%/g, this.server.serverInfo.onlineCount.toString())
      .replace(/%group_online%/g, this.server.serverInfo.onlineCount.toString())
      .replace(/%max_players%/g, this.server.serverInfo.maxPlayers.toString())
      .replace(/%motd%/g, this.server.serverInfo.motd)
      .replace(/%id%/g, this.server.serverInfo.serviceId.id.toString())
      .replace(/%host%/g, this.server.serverInfo.host)
      .replace(/%port%/g, this.server.serverInfo.port.toString())
      .replace(/%memory%/g, this.server.serverInfo.memory + 'MB')
      .replace(/%wrapper%/g, this.server.serverInfo.serviceId.wrapperId)
      .replace(/%extra%/g, String(this.server.serverInfo.serverConfig.extra))
      .replace(/%template%/g, this.server.serverInfo.template.name)
      .replace(/%group%/g, this.server.serverInfo.serviceId.group);
  }

  SaveConfig() {
    this._cloud.postMobConfig(JSON.parse(this._es.isElectronApp ? localStorage.getItem('currentUser') : this.cookie.get('currentUser')),
      this.mobConfig).subscribe(() => {
      this._notify.UpdateSuccesfully();
    }, error => {
      if ((error.status === 403)) {
        this._notify.AccessDenied();
      }
    });
  }

  getOffset(off) {
    return {
      'grid-column': Math.floor((this.mobConfig.startPoint - 1 + off) % 9) + 1,
      'grid-row': Math.floor((this.mobConfig.startPoint - 1 + off) / 9) + 1,
      'background-image': 'url(https://assets.madfix.me/projects/MaterialDesignWebInterface/pictures/blocks/' + this.mobConfig.itemLayout.itemId + '-' + this.mobConfig.itemLayout.subId + '.png)'
    };
  }

  getImage(item: KeyValue<number, MobItemLayout>) {
    return {
      'background-image': 'url(https://assets.madfix.me/projects/MaterialDesignWebInterface/pictures/blocks/' + item.value.itemId + '-' + item.value.subId + '.png)'
    };
  }

  getInv() {
    switch (this.mobConfig.inventorySize) {
      case 54: {
        return {
          'background-image': 'url(https://assets.madfix.me/projects/MaterialDesignWebInterface/pictures/inventory/' + this.mobConfig.inventorySize + '.png)',
          'font-family': 'Minecraft, serif',
          'color': 'black',
          'display': 'block',
          'background-size': '100% 100%',
          'background-repeat': 'no-repeat',
          'width': 'auto',
          'height': 'auto',
          'min-height': '444px',
          'min-width': '352px'
        };
      }
      case 45: {
        return {
          'background-image': 'url(https://assets.madfix.me/projects/MaterialDesignWebInterface/pictures/inventory/' + this.mobConfig.inventorySize + '.png)',
          'font-family': 'Minecraft, serif',
          'color': 'black',
          'display': 'block',
          'background-size': '100% 100%',
          'background-repeat': 'no-repeat',
          'width': 'auto',
          'height': 'auto',
          'min-height': '408px',
          'min-width': '352px'
        };
      }
      case 36: {
        return {
          'background-image': 'url(https://assets.madfix.me/projects/MaterialDesignWebInterface/pictures/inventory/' + this.mobConfig.inventorySize + '.png)',
          'font-family': 'Minecraft, serif',
          'color': 'black',
          'display': 'block',
          'background-size': '100% 100%',
          'background-repeat': 'no-repeat',
          'width': 'auto',
          'height': 'auto',
          'min-height': '372px',
          'min-width': '352px'
        };
      }
      case 27: {
        return {
          'background-image': 'url(https://assets.madfix.me/projects/MaterialDesignWebInterface/pictures/inventory/' + this.mobConfig.inventorySize + '.png)',
          'font-family': 'Minecraft, serif',
          'color': 'black',
          'display': 'block',
          'background-size': '100% 100%',
          'background-repeat': 'no-repeat',
          'width': 'auto',
          'height': 'auto',
          'min-height': '333px',
          'min-width': '352px'
        };
      }
      case 18: {
        return {
          'background-image': 'url(https://assets.madfix.me/projects/MaterialDesignWebInterface/pictures/inventory/' + this.mobConfig.inventorySize + '.png)',
          'font-family': 'Minecraft, serif',
          'color': 'black',
          'display': 'block',
          'background-size': '100% 100%',
          'background-repeat': 'no-repeat',
          'width': 'auto',
          'height': 'auto',
          'min-height': '300px',
          'min-width': '352px'
        };
      }
      case 9: {
        return {
          'background-image': 'url(https://assets.madfix.me/projects/MaterialDesignWebInterface/pictures/inventory/' + this.mobConfig.inventorySize + '.png)',
          'font-family': 'Minecraft, serif',
          'color': 'black',
          'display': 'block',
          'background-size': '100% 100%',
          'background-repeat': 'no-repeat',
          'width': 'auto',
          'height': 'auto',
          'min-height': '264px',
          'min-width': '352px'
        };
      }
    }

  }

  editItem(data: MobItemLayout) {
    this.dialog.open(EditItemComponent, {
      data: data,
    });
  }

  getFillItems() {
    if (this.mobConfig.startPoint > this.mobConfig.inventorySize) {
      return new Array(0);
    }
    return new Array(this.mobConfig.inventorySize - this.mobConfig.startPoint + 1);
  }

  getAddItems() {
    if (this.mobConfig.startPoint > this.mobConfig.inventorySize) {
      return new Array(this.mobConfig.inventorySize - this.mobConfig.defaultItemInventory.length);
    } else {
      return new Array(this.mobConfig.startPoint - this.mobConfig.defaultItemInventory.length - 1);
    }
  }

  getDefaultItem(): MobItemLayout {
    return {
      subId: 15,
      itemId: 160,
      itemName: '',
      lore: [' '],
      display: ' '
    };
  }

  add(defaultItem: MobItemLayout) {
    this.mobConfig.defaultItemInventory.push({
      key: this.mobConfig.defaultItemInventory.length,
      value: defaultItem
    });
  }

  removeItem(i: number) {
    this.mobConfig.defaultItemInventory.splice(i, 1);
    return false;
  }

  getMaxStart(): number {
    if (this.mobConfig.inventorySize > this.mobConfig.startPoint) {
      return this.mobConfig.inventorySize + 1;
    }
    return this.mobConfig.startPoint;
  }

  EditMob(mob: ServerMob) {
    const matDialogRef = this.dialog.open(EditMobComponent, {
      data: mob,
    });
    matDialogRef.afterClosed().subscribe(() => {
      this.refreshItems();
    });
  }

  getTranslation(key: string) {
    return this._.get(key);
  }
}

export interface MobDB {
  display: string;
  name: string;
  type: string;
  targetGroup: string;
  displayMessage: string;
  autoJoin: boolean;
  mob: ServerMob;
}
