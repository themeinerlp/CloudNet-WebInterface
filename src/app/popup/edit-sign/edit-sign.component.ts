import {Component, Inject, OnInit} from '@angular/core';
import {CloudConnectorService} from '../../_services/cloud-connector.service';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material';
import {NotificationsService} from '../../_services/notifications.service';
import {CookieService} from 'ngx-cookie-service';
import {ElectronService} from 'ngx-electron';
import {Sign} from '../../_models/sign';
import {DeleteInfoComponent} from '../delete-info/delete-info.component';

@Component({
  selector: 'app-edit-sign',
  templateUrl: './edit-sign.component.html',
  styleUrls: ['./edit-sign.component.css']
})
export class EditSignComponent implements OnInit {

  constructor(private _cloud: CloudConnectorService, public dialog: MatDialog, public _notify: NotificationsService,
              private cookie: CookieService, private _es: ElectronService,
              public dialogRef: MatDialogRef<EditSignComponent>, @Inject(MAT_DIALOG_DATA) public data: Sign) {
  }

  ngOnInit() {
  }

  save() {
    let b1 = false, b2 = false;
    this._cloud.postSignDelete(JSON.parse(this._es.isElectronApp ? localStorage.getItem('currentUser') : this.cookie.get('currentUser')),
      this.data.uniqueId).subscribe(() => {
      b1 = !b1;
    });
    this._cloud.postSignAdd(JSON.parse(this._es.isElectronApp ? localStorage.getItem('currentUser') : this.cookie.get('currentUser')),
      this.data).subscribe(() => {
      b2 = !b2;
    });
    if (b1 && !b2) {

    } else if (!b1 && b2) {

    } else if (!b1 && b2) {
      this._notify.AccessDenied();
    }
  }

  deleteSign() {
    this.dialog.open(DeleteInfoComponent, {
      data: this.data.uniqueId
    }).afterClosed().subscribe((next: boolean) => {
      if (next) {
        this._cloud.postSignDelete(
          JSON.parse(this._es.isElectronApp ? localStorage.getItem('currentUser') : this.cookie.get('currentUser')), this.data.uniqueId)
          .subscribe(() => {
          }, error => {
          if (error.status === 403) {
            this._notify.AccessDenied();
          }
        });
      }
    });
  }
}
