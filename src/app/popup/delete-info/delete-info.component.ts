import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';

@Component({
  selector: 'app-delete-info',
  templateUrl: './delete-info.component.html',
  styleUrls: ['./delete-info.component.css']
})
export class DeleteInfoComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<DeleteInfoComponent>, @Inject(MAT_DIALOG_DATA) public data: string) {
  }

  ngOnInit() {
  }

  onCloseClick() {
    this.dialogRef.close(false);
  }

  onDelete() {
    this.dialogRef.close(true);
  }
}
