/*
 * Copyright (c) 2019.
 * CloudNet-WebInterface von Phillipp Glanz ist lizenziert unter einer Creative Commons
 *  Namensnennung -
 *  Nicht kommerziell -
 *  Keine Bearbeitungen 4.0 International Lizenz.
 */

import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import 'rxjs/add/operator/map';
import {CloudNetWork} from '../_models';
import {BehaviorSubject} from 'rxjs';
import {NotificationsService} from './notifications.service';
import {CookieService} from 'ngx-cookie-service';
import {ElectronService} from 'ngx-electron';
import {User} from '../_models/user';
import {AppConfigService} from './app-config.service';

@Injectable(
  {
    providedIn: 'root',
  }
)
export class AuthenticationService {
  public static isUserLoggedIn: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  constructor(private http: HttpClient, public _notify: NotificationsService, private cookie: CookieService, private _es: ElectronService) {
    AuthenticationService.isUserLoggedIn.next(
      this._es.isElectronApp ? localStorage.getItem('currentCloud') !== null : this.cookie.check('currentCloud'));
  }

  login(username: string, password: string) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8',
      '-Xcloudnet-user': username,
      '-Xcloudnet-password': password
    });
    return this.http.post<any>((JSON.parse(this._es.isElectronApp ? localStorage.getItem('currentCloud') : this.cookie.get(
      'currentCloud')) as CloudNetWork).CloudURL + '/cloudnet/api/v2/auth', null, {headers: headers});
  }

  logout() {
    if (this._es.isElectronApp) {
      if (localStorage.getItem('tempCurrentUser')) {
        const oldUser = (JSON.parse(localStorage.getItem('tempCurrentUser'))) as User;
        localStorage.removeItem('tempCurrentUser');
        localStorage.setItem('currentUser', JSON.stringify(oldUser));
      } else {
        localStorage.removeItem('currentUser');
        localStorage.removeItem('currentCloud');
        AuthenticationService.isUserLoggedIn.next(false);
      }

    } else {
      const d = new Date();
      if (this.cookie.check('tempCurrentUser')) {
        const oldUser = JSON.parse(this.cookie.get('tempCurrentUser')) as User;
        this.cookie.delete('tempCurrentUser');
        this.cookie.set('currentUser', JSON.stringify(oldUser),
          new Date(d.getTime() + AppConfigService.settings.settings.timeout * 60 * 1000), '/');
      } else {
        this.cookie.delete('currentUser');
        this.cookie.delete('currentCloud');
        AuthenticationService.isUserLoggedIn.next(false);
      }
    }
  }
}
